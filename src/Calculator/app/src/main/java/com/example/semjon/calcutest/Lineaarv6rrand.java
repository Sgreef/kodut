package com.example.semjon.calcutest;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class Lineaarv6rrand extends Activity implements  OnClickListener {


    // deklareerin muutujad
    EditText x1, x2, y1, y2, a1, a2;
    TextView n2itaVastus;
    Button btnLahenda;

    @Override
    /** Kutsutakse välja kui actvity on tehtud */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lineaarv6rrand);


        // leian elemendid, mis xmlis
        x1 = (EditText) findViewById(R.id.x1);
        x2 = (EditText) findViewById(R.id.x2);
        y1 = (EditText) findViewById(R.id.y1);
        y2 = (EditText) findViewById(R.id.y2);
        a1 = (EditText) findViewById(R.id.a1);
        a2 = (EditText) findViewById(R.id.a2);


        n2itaVastus = (TextView) findViewById(R.id.n2itaVastus);
        btnLahenda = (Button) findViewById(R.id.btnLahenda);

        // nupu sean listeneriks
        btnLahenda.setOnClickListener(this);



    }// nupu listener
    public void onClick(View v) {
        float numx1 = 0;
        float numx2 = 0;
        float numy1 = 0;

        float numy2 = 0;
        float numa1 = 0;
        float numa2 = 0;

        float lahendX= 0;
        float lahendY= 0;

        // kontrollin kas väljad täidetud
        if (TextUtils.isEmpty(x1.getText().toString())
                || TextUtils.isEmpty(x2.getText().toString()) || TextUtils.isEmpty(y1.getText().toString())
                || TextUtils.isEmpty(y2.getText().toString())|| TextUtils.isEmpty(a1.getText().toString())
                || TextUtils.isEmpty(a2.getText().toString())) {
            return;
        }

        // saan kasutaja sisestatud arvud
        numx1 = Float.parseFloat(x1.getText().toString());
        numx2 = Float.parseFloat(x2.getText().toString());
        numy1 = Float.parseFloat(y1.getText().toString());
        numy2 = Float.parseFloat(y2.getText().toString());
        numa1 = Float.parseFloat(a1.getText().toString());
        numa2 = Float.parseFloat(a2.getText().toString());


        // lineaarV klassist kutsun välja meetodi mis lahendab lineaarvõrrandi süsteemi
        lahendX= LineaarV.Lineaar(numx1,numy1,numa1,numx2,numy2,numa2)[0];
        lahendY= LineaarV.Lineaar(numx1,numy1,numa1,numx2,numy2,numa2)[1];

        // annan n2itaVastus textWievele lahendite väärtused
        n2itaVastus.setText("x1= " + lahendX + "   x2= " + lahendY);

    }



}
