package com.example.semjon.calcutest;

/**
 * Created by Semjon on 20-Nov-15.
 * valemid erinevate nuppude jaoks
 */
public class Matemaatika {

    // liitmise meetod, tagastab kahe arvu summat
    public static float Add(float x, float y) {
        float result = x + y;
        return result;

// lahutamise meetod, tagastab kahe arvu vahet
    }

    public static float Substract(float x, float y) {
        float result = x - y;
        return result;

// korrutamise meetod, tagastab kahe arvu korrutist
    }

    public static float Multiply(float x, float y) {
        float result = x * y;
        return result;

// jagamise meetod, tagastab kahe arvu jagatist
    }

    public static float Divide(float x, float y) {
        float result = x / y;
        if (x == y) {
            result = 1;
        }
        return result;
    }

    // astendamise meetod, tagastab x astmes y
    public static float Pow(float x, float y) {
        float result = 1;
        for (int i = 0; i < y; i++) {
            result *= x;
        }
        return result;
    }

    // juurimis meetod, tagastab x, mis on y juures
    public static float Root(float x, float y) {
        float result = (float) Math.pow(Math.E, Math.log(x) / y);
        return result;
    }


}
