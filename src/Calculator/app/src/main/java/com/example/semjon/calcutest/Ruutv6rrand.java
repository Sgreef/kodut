package com.example.semjon.calcutest;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

// ruutv6rrandi mainactivity
public class Ruutv6rrand extends Activity implements  OnClickListener {

    // muutujad
    EditText a, b, c;
    TextView kuvaVastus;
    Button btnAarvuta;


//meetod, mis kutsutakse activitys välja esimesena
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ruutv6rrand);

        a = (EditText) findViewById(R.id.a);
        b = (EditText) findViewById(R.id.b);
        c = (EditText) findViewById(R.id.c);

        kuvaVastus = (TextView) findViewById(R.id.kuvaVastus);
        btnAarvuta = (Button) findViewById(R.id.btnArvuta);

    // sean buttonile listeneri
        btnAarvuta.setOnClickListener(this);



    }
    public void onClick(View v) {
        float numA = 0;
        float numB = 0;
        float numC = 0;

        float vastus1= 0;
        float vastus2= 0;

        // kontrollin kas väljad tühjad
        if (TextUtils.isEmpty(a.getText().toString())
                || TextUtils.isEmpty(b.getText().toString()) || TextUtils.isEmpty(b.getText().toString())) {
            return;
        }
// annan muutujatele väärtused , mis kasutaja sisestas
        numA = Float.parseFloat(a.getText().toString());
        numB = Float.parseFloat(b.getText().toString());
        numC = Float.parseFloat(c.getText().toString());

// tagastan ruutV klassist ruutvõrrandi lahendid
        vastus1 = ruutV.Ruutv6rrand(numA,numB,numC)[0];
        vastus2 = ruutV.Ruutv6rrand(numA,numB,numC)[1];

        // annan teksti vastuse näitamiseks tvResult elemendile
        kuvaVastus.setText("x1= " + vastus1 + "   x2= " + vastus2);

    }



}
