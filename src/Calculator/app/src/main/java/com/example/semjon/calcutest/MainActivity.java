package com.example.semjon.calcutest;
/**
 * Created by Semjon on 20-Nov-15.
 * inspiratsioon saadud https://www.youtube.com/watch?v=Xl1x8eazbrM videost
 */
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

//MainActivity klass
public class MainActivity extends Activity implements OnClickListener {

// deklareerin muutujad
    EditText etNum1, etNum2;

    Button btnAdd, btnSub, btnMult, btnDiv, btnPow,btnRoot, btnRV, btnLV;
    TextView tvResult;
    String oper = "";

    /** Kutsutakse välja kui actvity on tehtud */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // leian elemendid, mis xmlis
        etNum1 = (EditText) findViewById(R.id.etNum1);
        etNum2 = (EditText) findViewById(R.id.etNum2);

        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnSub = (Button) findViewById(R.id.btnSub);
        btnMult = (Button) findViewById(R.id.btnMult);
        btnDiv = (Button) findViewById(R.id.btnDiv);
        btnPow = (Button) findViewById(R.id.btnPow);
        btnRoot = (Button) findViewById(R.id.btnRoot);
        btnRV = (Button) findViewById(R.id.btnRV);
        btnLV = (Button) findViewById(R.id.btnLV);

        tvResult = (TextView) findViewById(R.id.tvResult);

        // nupud sean listeneriks
        btnAdd.setOnClickListener(this);
        btnSub.setOnClickListener(this);
        btnMult.setOnClickListener(this);
        btnDiv.setOnClickListener(this);
        btnPow.setOnClickListener(this);
        btnRoot.setOnClickListener(this);

        //Ruutvõrrandi nupu listener
        btnRV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ruutv6rrandAken();
            }
        });

        //Lineaarvõrrandi nupu listener
        btnLV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                lineaarv6rrandAken();
            }
        });

    }
    // Meetod mis kutsub välja Ruutvõrrand activity
    public void ruutv6rrandAken() {

        Intent intent = new Intent(this, Ruutv6rrand.class);
        startActivity(intent);
    }

    // Meetod mis kutsub välja Lineaarvõrrandi activity
    public void lineaarv6rrandAken() {
        Intent intent = new Intent(this, Lineaarv6rrand.class);
        startActivity(intent);
    }


    // Kasutatakse kui vajutatakse mingisugust arvutamisnuppu
    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        float num1 = 0;
        float num2 = 0;
        float result = 0;

        // kontrollin kas input field-id täidetud
        if (TextUtils.isEmpty(etNum1.getText().toString())
                || TextUtils.isEmpty(etNum2.getText().toString())) {
            return;
        }

        // annan num1 ja num2 väärtused, mis kasutaja sisestas feild-idesse
        num1 = Float.parseFloat(etNum1.getText().toString());
        num2 = Float.parseFloat(etNum2.getText().toString());

        // erinevate nupude vajutamisel teen asju, mis igas case-is
        //valemid kutsun välja Matemaatika klassist
        switch (v.getId()) {
            case R.id.btnAdd:
                oper = "+";
                result = Matemaatika.Add(num1,num2);
                break;
            case R.id.btnSub:
                oper = "-";
                result = Matemaatika.Substract(num1,num2);
                break;
            case R.id.btnMult:
                oper = "*";
                result = Matemaatika.Multiply(num1,num2);
                break;
            case R.id.btnDiv:
                oper = "/";
                result = Matemaatika.Divide(num1,num2);
                break;
            case R.id.btnPow:
                oper = "^";
                result = Matemaatika.Pow(num1,num2);
                break;
            case R.id.btnRoot:
                oper="Root";
                result= Matemaatika.Root(num1, num2);
                break;
            default:
                break;
        }

        // annan teksti vastuse näitamiseks tvResult elemendile
        tvResult.setText(num1 + " " + oper + " " + num2 + " = " + result);
    }
}
